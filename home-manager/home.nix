{
  config,
  lib,
  pkgs,
  ...
}: let
  dtName = "Dimitri Torterat";
  dtEmail = "kra@diti.me";
  dtGpgKeyC = "4427860F1FFAB6BFE3ABBB6F29C98762776776D1";
  dtGpgKeyS = "49DE9CD0B3FF38EF3C590870FC1D2FAEA31C4857";
in {
  accounts.email = {
    accounts.Fastmail = {
      address = "${dtEmail}";
      flavor = "fastmail.com";
      gpg = {key = "${dtGpgKeyC}";};
      primary = true;
      realName = "${dtName}";
    };
  };
  editorconfig = {
    enable = true;
    settings = {
      "*" = {
        charset = "utf-8";
        end_of_line = "lf";
        indent_size = 2;
        indent_style = "space";
        insert_final_newline = true;
        trim_trailing_whitespace = true;
      };
      "*.ps{,d,m}1{,xml}" = {
        end_of_line = "crlf";
        indent_size = 4;
        indent_style = "space";
        max_line_width = 115;
      };
      "Makefile" = {
        end_of_line = "lf";
        indent_size = 4;
        indent_style = "tab";
      };
    };
  };
  home = {
    username = "diti";
    homeDirectory = "/home/diti";
    stateVersion = "23.11";
    enableNixpkgsReleaseCheck = true;

    file = {
      ".digrc".text = ''
        +nocmd
        +nocomments
        +noquestion
        +nostats
        +recurse
      '';
      ".sqliterc".text = ''
        .headers ON
        .mode box --quote on --wordwrap on
      '';
    };
    packages = with pkgs; [
      alejandra
      aria2
      bind.dnsutils
      bitwarden
      dogdns
      duf
      elixir
      ffmpeg-full
      haskellPackages.arbtt
      jaq
      litecli
      pgcli
      podman
      ripgrep
      sqlite
      tealdeer
      uni
      vivid
      xh
      xsv
      yt-dlp
    ];
    sessionPath = ["$HOME/.local/bin" "$HOME/.mix/escripts"];
    sessionVariables = {
      EDITOR = "nvim";
      ERL_AFLAGS = "-kernel shell_history enabled";
      GNUPGHOME = "${config.xdg.configHome}/gnupg";
      LANG = "en_US.UTF-8";
      LC_ALL = "en_US.UTF-8";
      LS_COLORS = "$(${pkgs.vivid}/bin/vivid generate gruvbox-dark)";
      MANPAGER = "sh -c 'col --no-backspaces --spaces | ${pkgs.bat}/bin/bat --language man --plain'";
      MANROFFOPT = "-c"; # Disable color when `man` calls `groff`; `bat` handles the coloring.
      USE_EDITOR = "${config.home.sessionVariables.EDITOR}";
      VISUAL = "${config.home.sessionVariables.EDITOR}";
      WGETRC = "${config.xdg.configHome}/wget/wgetrc";
      XDG_DATA_DIRS = "$HOME/.nix-profile/share:$XDG_DATA_DIRS";
    };
    shellAliases = let
      ezaLongOpts = builtins.concatStringsSep " " [
        "--classify"
        "--group"
        "--group-directories-first"
        "--time-style=long-iso"
      ];
    in {
      cat = "bat --paging=never --plain";
      la = "eza --all";
      ll = "eza --long ${ezaLongOpts}";
      lla = "eza --all --long ${ezaLongOpts}";
      ls = "eza";
      path = ''echo "$PATH" | tr : \\n'';
    };
  };
  programs = {
    bat = {
      enable = true;
      config = {
        map-syntax = ["*.livemd:Markdown"];
        paging = "always";
        style = "full";
        theme = "gruvbox-dark";
      };
    };
    direnv = {
      enable = true;
      nix-direnv.enable = true;
    };
    eza.enable = true;
    git = {
      enable = true;
      difftastic = {
        enable = true;
        background = "dark";
      };
      extraConfig = {
        diff = {
          mnemonicprefix = true;
        };
        commit = {
          template = builtins.toFile "git-commit-template.txt" ''

            ################################################# ← 50              72 ↓
            ########################################################################
            # Summarize the change in 50 characters or less
            #
            # Empty line between the first (subject) line and the body.
            #
            # The subject line should be imperative (“this commit will …” should be
            # a valid sentence), start with an uppercase letter, lack an end period.
            #
            # Body text, wrapped at 72 characters, should provide a more detailed
            # description of the change in the following lines, breaking paragraphs
            # where needed.
            #
            #  * Don’t assume the reviewer knows what the original problem was.
            #  * Don’t assume the reviewer has internet access.
            #  * Don’t assume the code is self-evident/self-documenting.
            #  * Describe WHY a change is being made (intent/motivation).
            #  * Read the commit message to see if it hints at improved structure
            #    (in which case the change should be separate smaller commits).
            #
            # Example paragraphs: original problem; functional change being made;
            # result of the change; scope for future improvement; trailer(s).
            #
            # Empty line between the body and the trailer(s).
            # Example trailers:⠀
            #
            # Co-Authored-By: Alice Owl <alice.owl@example.org>
            # Co-Authored-By: Bob Penguin <bob.penguin@example.org>
            # Fixes: #123
            # See also: #456, #789
            ########################################################################
          '';
        };
        help = {
          autocorrect = 10;
        };
        init = {
          defaultBranch = "master"; # Git < 2.28 hardcodes `HEAD` to `refs/heads/master`
        };
        merge = {
          ff = "only";
        };
        pull = {
          ff = "only";
        };
        push = {
          gpgSign = "if-asked";
        };
      };
      package = pkgs.gitAndTools.gitFull;
      signing = {
        key = "${dtGpgKeyS}!";
        signByDefault = true;
      };
      userName = "${dtName}";
      userEmail = "${dtEmail}";
    };
    gpg = {
      enable = true;
      homedir = "${config.xdg.configHome}/gnupg";
      settings = {
        ask-cert-level = true;
        cert-notation = "pgp@diti.me=https://diti.me/pgp/certs/%f.notes.asc";
        cert-policy-url = "https://diti.me/pgp/#policy";
        default-key = "${dtGpgKeyC}";
        keyid-format = "long";
        keyserver = "hkps://keys.openpgp.org";
        keyserver-options = "no-honor-keyserver-url";
        list-options = "show-keyserver-urls show-notations show-policy-urls show-uid-validity show-usage";
        no-greeting = true;
        verify-options = "show-keyserver-urls show-notations show-policy-urls show-uid-validity";
      };
    };
    home-manager.enable = true;
    htop = {
      enable = true;
      settings = {
        tree_view = 1;
      };
    };
    kitty = {
      enable = true;
      extraConfig = ''
      '';
      shellIntegration = {
        enableBashIntegration = true;
        enableZshIntegration = true;
      };
      theme = "Gruvbox Dark";
    };
    neovim = {
      enable = true;
      extraConfig = ''
        let g:netrw_banner = 0
        let g:netrw_liststyle = 3
        nnoremap <silent> <CR> :nohlsearch<CR>
        set list
        set mouse=v
        set nofoldenable
        set number
        set termguicolors
        autocmd VimEnter * ++nested colorscheme gruvbox
      '';
      plugins = with pkgs.vimPlugins; let
        context-vim = pkgs.vimUtils.buildVimPlugin {
          name = "context.vim";
          src = pkgs.fetchFromGitHub {
            owner = "wellle";
            repo = "context.vim";
            rev = "108644e146c864995288dee7dacf219267917ac1";
            sha256 = "vStpwqEZVbNzswvXu549HXPmbLHrANBsVw978SfzRWs=";
          };
        };
        vim-powershell = pkgs.vimUtils.buildVimPlugin {
          name = "vim-powershell";
          src = pkgs.fetchFromGitHub {
            owner = "zigford";
            repo = "vim-powershell";
            rev = "7d0c8581e774cab8198bafa936e231b6f4c634e6";
            sha256 = "Pvg6nS5XBDecg3dIzEzZyBrXSPuKz62oC5ThBFQ4TB0=";
          };
        };
      in [
        context-vim
        editorconfig-nvim
        gruvbox-community
        {
          plugin = vim-airline;
          config = ''
            let g:airline_extensions = ['whitespace']

            if !exists('g:airline_symbols')
              let g:airline_symbols = {}
            endif
            let g:airline_left_sep = '''
            let g:airline_right_sep = '''
            let g:airline_symbols.crypt = '🔒'
            let g:airline_symbols.whitespace = '␠'

            call airline#parts#define_raw('dtlinecol', '%l:%v')
            if airline#util#winwidth() >= 80
              call airline#parts#define_raw('dtmaxlinenr', ' (%L)')
            else
              call airline#parts#define_raw('dtmaxlinenr', ''')
            endif
            let g:airline_section_z = airline#section#create(['%p%% ', 'dtlinecol', 'dtmaxlinenr'])
          '';
        }
        vim-elixir
        vim-nix
        vim-powershell
        {
          plugin = wilder-nvim;
          config = ''
            function! s:wilder_init() abort
              call wilder#setup({
              \   'enable_cmdline_enter': 0,
              \   'modes': [':', '/', '?'],
              \ })
              call wilder#set_option('pipeline', [
              \   wilder#debounce(10),
              \   wilder#branch(
              \     wilder#cmdline_pipeline({
              \       'language': 'vim',
              \     }),
              \   ),
              \ ])
              call wilder#set_option('renderer', wilder#wildmenu_renderer({'apply_incsearch_fix': 0}))
            endfunction
            autocmd CmdlineEnter * ++once call s:wilder_init() | call wilder#main#start()
          '';
        }
      ];
      # Only loaded if programs.neovim.extraConfig is set
      viAlias = true;
      vimAlias = true;
      vimdiffAlias = true;
    };
    ssh = {
      enable = true;
      extraConfig = ''
        VisualHostKey yes
      '';
      matchBlocks = {
        "flavi" = {
          hostname = "dook.business";
          user = "diti";
          port = 6467;
        };
        "toss" = {
          hostname = "home.diti.me";
          user = "Diti";
          port = 222;
        };
      };
    };
    zsh = {
      autocd = true;
      defaultKeymap = "emacs";
      dirHashes = {
        nixconfig = "${config.xdg.configHome}/nixpkgs";
        projs = "$HOME/var/proj";
      };
      dotDir = ".config/zsh";
      enable = true;
      enableAutosuggestions = true;
      enableCompletion = true;
      syntaxHighlighting = {
        enable = true;
        highlighters = [
          "main"
        ];
        styles = {
          alias = "fg=magenta,bold";
        };
      };
      envExtra = ''
        export WORDCHARS="''${WORDCHARS/\//}"
      '';
      history.path = "${config.xdg.dataHome}/zsh/zsh_history";
      initExtra = ''
        function move-task-to-foreground() {
          if [[ $#BUFFER == 0 ]]; then
            fg >/dev/null 2>&1 && zle redisplay
          else
            zle push-input
          fi
        }

        stty -ixon # Disable XON/XOFF output control (^S/^Q)

        autoload -Uz edit-command-line && zle -N edit-command-line && bindkey '^X^E' edit-command-line
        bindkey '^R' history-incremental-pattern-search-backward
        bindkey '^S' history-incremental-pattern-search-forward
        zle -N move-task-to-foreground && bindkey '^Z' move-task-to-foreground
        bindkey '^ ' autosuggest-accept
        bindkey '^\n' accept-and-hold
      '';
      initExtraBeforeCompInit = ''
        setopt ALIASES
        setopt CORRECT
        setopt IGNORE_EOF
        setopt INTERACTIVECOMMENTS
        setopt PROMPT_SUBST
        setopt RM_STAR_WAIT

        fpath=(
          "${pkgs.nix-zsh-completions}/share/zsh/site-functions"
          "${pkgs.podman}/share/zsh/site-functions"
          "${pkgs.zsh-completions}/share/zsh/site-functions"
          $fpath)

        autoload -Uz add-zsh-hook vcs_info
        precmd_nix_shell() {
          [[ -v IN_NIX_SHELL ]] && dt_prompt_='[%(!.#.$)]' || dt_prompt_='%(!.#.$)'
        }
        precmd_vcs_info() { vcs_info }
        precmd_functions+=( precmd_nix_shell precmd_vcs_info )
        PROMPT='$dt_prompt_ '
        RPROMPT='%~ $vcs_info_msg_0_'
        zstyle ':vcs_info:git:*' formats '%b'

        # eval "$(op completion zsh)";
      '';
      profileExtra = ''
        test -f "$HOME/.nix-profile/etc/profile.d/nix.sh" && source "$_"
      '';
      shellAliases = {
        history = "fc -il 1";
        reload = "source ${config.xdg.configHome}/zsh/.zshrc";
        ytdl = "noglob yt-dlp";
      };
    };
  };
  services = {
    gpg-agent = {
      enable = true;
      enableScDaemon = true;
      enableSshSupport = true;
      grabKeyboardAndMouse = true;
      pinentryFlavor = "curses";
      sshKeys = [
        "8F1DC3F0D485EDE185012A9E444842E6D6BEB898" # openpgp:0x423EC65F5BCBD4DA
        "A87F050B740C744A021CFA294DCC1A18911ADCC7" # diti@martin.home.diti.me
      ];
    };
  };
  #systemd.user.startServices = "sd-switch";
  xdg = {
    enable = true;
    configFile = {
      "curl/curlrc".text = ''
        connect-timeout = 60
        referer = ";auto"
      '';

      "wget/wgetrc".text = ''
        adjust_extension = on
        content-disposition = on
        follow_ftp = on
        robots = off
        secureprotocol = PFS
        timeout = 60
        timestamping = on
        tries = 3
      '';

      "yt-dlp/config".text = ''
        --compat-options multistreams
        --downloader aria2c
        --embed-chapters
        --embed-metadata
        --embed-subs
        --embed-thumbnail
        --merge-output-format mkv
        --output "$HOME/var/download/%(webpage_url_domain|youtube.com)s/%(uploader|)#S/%(upload_date>%Y-%m-%d)s_%(title)#S_%(id)s.%(ext)s"
        --sponsorblock-mark all
        --sponsorblock-remove sponsor,outro,interaction
        --sub-format ass
        --sub-langs 'en.*,fr,pl'
      '';
    };
    userDirs = {
      enable = true;
      createDirectories = true;
      desktop = "${config.home.homeDirectory}/var/desktop";
      documents = "${config.home.homeDirectory}/opt/docs";
      download = "${config.home.homeDirectory}/var/download";
      music = "${config.home.homeDirectory}/opt/art/music";
      pictures = "${config.home.homeDirectory}/opt/art/visual/picture";
      publicShare = "${config.home.homeDirectory}/var/public";
      videos = "${config.home.homeDirectory}/opt/art/visual/video";
      templates = "${config.home.homeDirectory}/opt/templates";
    };
  };
  xresources.extraConfig = builtins.readFile (pkgs.fetchFromGitHub {
      owner = "gruvbox-community";
      repo = "gruvbox-contrib";
      rev = "61a8802045fb74cbab72a8b2dec1d35318f1ff8c";
      sha256 = "sha256-Yp150GttnemwwV6uupcSC0fVij80hcvWxPwseJ9oWWg=";
    }
    + "/xresources/gruvbox-dark.xresources");
}
