{
  inputs,
  lib,
  config,
  pkgs,
  ...
}: {
  imports = [
    ./hardware-configuration.nix # nixos-generate-config
    inputs.nixos-hardware.nixosModules.common-cpu-intel
    inputs.nixos-hardware.nixosModules.common-gpu-nvidia-nonprime
    inputs.nixos-hardware.nixosModules.common-pc-ssd

    inputs.home-manager.nixosModules.home-manager
    inputs.sops-nix.nixosModules.sops
  ];

  sops.defaultSopsFile = ../secrets.yaml;
  #sops.defaultSopsFormat = "yaml";

  sops.age.sshKeyPaths = ["/etc/ssh/ssh_host_ed25519_key"];
  sops.gnupg.sshKeyPaths = [];
  sops.secrets."users/diti/hashedPassword".neededForUsers = true;

  nix = {
    gc = {
      automatic = true;
      dates = "weekly";
      options = "--delete-older-than 30d";
      randomizedDelaySec = "45min";
    };
    nixPath = lib.mapAttrsToList (key: value: "${key}=${value.to.path}") config.nix.registry;
    registry = lib.mapAttrs (_: value: {flake = value;}) inputs;
    settings = {
      auto-optimise-store = true;
      experimental-features = "nix-command flakes";
      trusted-users = ["@wheel"];
    };
  };
  nixpkgs = {
    config.allowUnfree = true;
    overlays = [inputs.blender-bin.overlays.default];
  };

  boot = {
    loader = {
      efi = {
        canTouchEfiVariables = true;
      };
      systemd-boot = {
        enable = true;
        editor = false;
        configurationLimit = 10;
      };
      timeout = 2;
    };
  };
  environment = {
    sessionVariables = {
      MOZ_USE_XINPUT2 = "1";
    };
    shells = [pkgs.zsh];
    systemPackages = with pkgs; [
      ddcutil
      git
      haruna
      krita
      ncdu
      neovide
      neovim
      ocs-url
      tailscale
      xclip
      zsh
    ];
  };
  hardware = {
    bluetooth = {
      enable = true;
      powerOnBoot = true;
    };
    i2c.enable = true;
    opengl = {
      enable = true;
      driSupport = true;
      driSupport32Bit = true;
    };
    pulseaudio.enable = false;
  };
  i18n = {
    defaultLocale = "en_US.UTF-8";
    extraLocaleSettings = {
      LC_ADDRESS = "fr_FR.UTF-8";
      LC_IDENTIFICATION = "fr_FR.UTF-8";
      LC_MEASUREMENT = "fr_FR.UTF-8";
      LC_MONETARY = "fr_FR.UTF-8";
      LC_NAME = "fr_FR.UTF-8";
      LC_NUMERIC = "fr_FR.UTF-8";
      LC_PAPER = "fr_FR.UTF-8";
      LC_TELEPHONE = "fr_FR.UTF-8";
      LC_TIME = "fr_FR.UTF-8";
    };
  };
  networking = {
    domain = "home.diti.me";
    firewall = {
      enable = true;
      trustedInterfaces = ["tailscale0"];
    };
    hostName = "dtphoudespcu";
    networkmanager.enable = true;
  };
  programs = {
    firefox = {
      enable = true;
      languagePacks = ["en-US" "fr"];
      package = (pkgs.wrapFirefox.override {libpulseaudio = pkgs.libpressureaudio;}) pkgs.firefox-unwrapped {};
      preferences = {
        "browser.search.defaultenginename" = "DuckDuckGo";
        "browser.newtabpage.activity-stream.feeds.topsites" = false;
        "browser.newtabpage.activity-stream.showSponsoredTopSites" = false;
        "browser.search.widget.inNavBar" = true;
        "browser.startup.page" = 3; # previous-session
        "browser.toolbars.bookmarks.visibility" = "newtab";
        "browser.urlbar.suggest.quicksuggest.sponsored" = false;
        "extensions.pocket.enabled" = false;
        "keyword.enabled" = false; # Do not consider potential hostnames as search terms
        "security.insecure_connection_text.enabled" = true;
        "security.insecure_connection_text.pbmode.enabled" = true;
        "security.osclientcerts.autoload" = true;
        "signon.rememberSignons" = false;
        "widget.use-xdg-desktop-portal.file-picker" = 1;
      };
    };
    light.enable = true;
    steam = {
      enable = true;
      dedicatedServer.openFirewall = true;
      remotePlay.openFirewall = true;
    };
    zsh.enable = true;
  };
  security.rtkit.enable = true;
  services = {
    arbtt.enable = true;
    caddy = {
      enable = true;
      user = "root"; # FIXME https://tailscale.com/kb/1190/caddy-certificates#provide-non-root-users-with-access-to-fetch-certificate
      virtualHosts."dtphoudespcu.ermine-dab.ts.net" = {
        extraConfig = ''
          templates
          respond "{{.RemoteIP}}"
        '';
        serverAliases = ["dtphoudespcu:80"];
      };
    };
    openssh = {
      enable = true;
      settings = {
        PermitRootLogin = "no";
        PasswordAuthentication = false;
      };
    };
    pipewire = {
      enable = true;
      alsa = {
        enable = true;
        support32Bit = true;
      };
      pulse.enable = true;
    };
    printing.enable = true;
    tailscale = {
      enable = true;
      openFirewall = true;
    };
    xserver = {
      enable = true;
      desktopManager.plasma5.enable = true;
      displayManager.sddm.enable = true;
      layout = "us";
      #libinput = {
      #  enable = true;
      #  touchpad = {
      #    disableWhileTyping = true;
      #    naturalScrolling = true;
      #  };
      #};
      xkbVariant = "altgr-intl";
    };
  };
  sound.enable = true;
  system = {
    autoUpgrade = {
      enable = true;
      dates = "daily";
      flags = [
        # See `nix3-flake-lock(1)`
        "--commit-lock-file"
        "--recreate-lock-file"
      ];
      # If local (`git+file`), we can use `--commit-lock-file` (but local flakes get out of sync on other machines).
      # If remote (`git+https`), we have to use `no-write-lock-file` (and manually update, commit and push lockfile).
      flake = "git+https://gitlab.com/Diti/nix-config.git";
      operation = "boot"; # FIXME: We want "switch" on servers
      persistent = true;
      randomizedDelaySec = "45min";
    };
    includeBuildDependencies = true;
    stateVersion = "23.11"; # https://nixos.wiki/wiki/FAQ/When_do_I_update_stateVersion
  };
  time.timeZone = "Europe/Paris";
  users = {
    defaultUserShell = pkgs.zsh;
    mutableUsers = false;
    users = {
      diti = {
        description = "Dimitri Torterat";
        extraGroups = ["networkmanager" "video" "wheel"];
        hashedPasswordFile = config.sops.secrets."users/diti/hashedPassword".path;
        isNormalUser = true;
        openssh.authorizedKeys.keys = [
          "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKll5ejN57ynUhC25gJmX0fOnphc+amNY6BOjfDDcmYw openpgp:423EC65F5BCBD4DA"
        ];
        packages = with pkgs; [
          blender
          element-desktop
          (discord.override {
            withOpenASAR = true;
            withVencord = true;
          })
          signal-desktop
          tdesktop
        ];
        shell = pkgs.zsh;
      };
    };
  };
  virtualisation.vmVariant = {
    users.users.virtualpeng = {
      description = "Virtual Penguin";
      isNormalUser = true;
      password = "virtualpeng";
    };
    virtualisation = {
      cores = 4;
      memorySize = 8186;
    };
  };
}
