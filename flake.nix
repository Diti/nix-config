{
  description = "Diti’s Nix configuration files.";

  inputs = {
    blender-bin = {
      inputs.nixpkgs.follows = "nixpkgs";
      url = github:edolstra/nix-warez?dir=blender;
    };
    flake-utils = {
      url = github:numtide/flake-utils;
    };
    home-manager = {
      inputs = {
        nixpkgs.follows = "nixpkgs";
      };
      url = github:nix-community/home-manager;
    };
    nix-index-database = {
      inputs.nixpkgs.follows = "nixpkgs";
      url = github:Mic92/nix-index-database;
    };
    nixos-hardware.url = github:nixos/nixos-hardware;
    nixpkgs.url = github:nixos/nixpkgs/nixos-unstable;
    sops-nix = {
      # Follows the unstable; see `nix flake metadata`
      inputs.nixpkgs.follows = "nixpkgs";
      url = github:Mic92/sops-nix;
    };
  };

  outputs = {self, ...} @ inputs: {
    #devShells = import ./shell.nix {pkgs = inputs.nixpkgs;};
    homeConfigurations = {
      "diti" = inputs.home-manager.lib.homeManagerConfiguration {
        modules = [
          inputs.nix-index-database.hmModules.nix-index
          ./home-manager/home.nix
        ];
        pkgs = inputs.nixpkgs.legacyPackages."x86_64-linux";
        extraSpecialArgs = {inherit inputs;};
      };
    };
    nixosConfigurations = {
      "dtphoudespcu" = inputs.nixpkgs.lib.nixosSystem {
        modules = [
          ./nixos/configuration.nix
          #inputs.nix-index-database.nixosModules.nix-index # FIXME: conflicts with home-manager
          {
            home-manager.useGlobalPkgs = true;
            home-manager.useUserPackages = true;
            home-manager.users.diti = import ./home-manager/home.nix;
            nixpkgs.hostPlatform = "x86_64-linux";
          }
        ];
        specialArgs = {inherit inputs;};
      };
    };
  };
}
